<?php
/**
 * @file
 * This defines the administrative interface for the overmind module.
 */

function overmind_settings() {
  $header = 'This is the Overmind settings form. There is nothing here yet...';

  //radio buttons for security mode
  //select list for rsa or dsa key
  //select list for how many bits

/*
  // generate a 1024 bit rsa private key, returns a php resource
  $privateKey = openssl_pkey_new(array(
    'private_key_bits' => 1024,
	'private_key_type' => OPENSSL_KEYTYPE_RSA,
  ));
  openssl_pkey_export_to_file($privateKey, '/path/to/privatekey', $passphrase);

  // get the public key $keyDetails['key'] from the private key;
  $keyDetails = openssl_pkey_get_details($privateKey);
  file_put_contents('/path/to/publickey', $keyDetails['key']);
*/

  return $header;
}
