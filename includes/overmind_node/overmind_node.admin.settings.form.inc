<?php
/**
 * @file
 * This defines the administrative interface for the overmind_node module.
 */

function overmind_node_settings() {
  $header = 'This is the Overmind node settings form. There is nothing here yet...';

  //Add support for pushing updates when a module is enabled or disabled
  //hook_modules_enabled
  //hook_modules_disabled

  return $header;
}
