<?php
/**
 * @file
 * This defines functions for overmind node to collect data.
 */

/**
 * Collects overmind node data.
 *
 * @return string
 *   A string of serialized overmind node data, optionally encrypted.
 */
function overmind_node_data() {
  $overmind = array();
  // Overmind data meta data.
  $overmind['overmind-info']['version'] = 1;
  $overmind['overmind-info']['schema'] = 1;

  // Overmind core data.
  $overmind['data']['core']['cron'] = _overmind_node_get_cron();
  $overmind['data']['core']['watchdog'] = _overmind_node_get_wd();
  $overmind['data']['core']['modules'] = _overmind_node_get_modules();
  $overmind['data']['core']['ufa'] = _overmind_node_get_ufa();
  $overmind['data']['core']['dbupdate'] = _overmind_node_get_dbupdate();

  // Allow other modules to define their own data to report.
  $modules = module_implements('overmind_node_data', $sort = FALSE, $reset = FALSE);
  if (!empty($modules)) {
    foreach ($modules as $module) {
      $overmind['data'][$module] = call_user_func("{$module}_overmind_node_data", NULL);
    }
  }

  $overmind = serialize($overmind);
  if (OVERMIND_NODE_ENCRYPT_DATA) {
    // @todo get key.
    openssl_public_encrypt($overmind, $crypted, $key);
    $ret = $crypted;
  }
  else {
    $ret = $overmind;
  }
  print $ret;
  exit;
}

/* Functions to group and format results. */

/**
 * Function group for cron overmind node data.
 *
 * Gathers data relating to cron.
 *
 * @return array
 *   An array of result data from cron data functions.
 */
function _overmind_node_get_cron() {
  return array(
    'Cron last run' => _overmind_node_get_cron_last_run(),
  );
}

/**
 * Function group for watchdog overmind node data.
 *
 * Gathers data relating to watchdog.
 *
 * @return array
 *   An array of result data from watchdog data functions.
 */
function _overmind_node_get_wd() {
  return array(
    'Total rows in watchdog' => _overmind_node_get_wd_rows(),
    'Logs go back' => _overmind_node_get_wd_oldest(),
  );
}

/**
 * Function group for module overmind node data.
 *
 * Gathers data relating to modules.
 *
 * @return array
 *   An array of result data from module data functions.
 */
function _overmind_node_get_modules() {
  return array(
    'modules' => _overmind_node_get_modules_status(),
  );
}

/**
 * Function group for update free access check.
 *
 * Checks update free access.
 *
 * @return array
 *   An array of result data from update free access check.
 */
function _overmind_node_get_ufa() {
  return array(
    'Update Free Access' => _overmind_node_get_ufa_status(),
  );
}

/**
 * Function group for database update check.
 *
 * Checks if there are any database updates pending.
 *
 * @return array
 *   An array of result data from database update check.
 */
function _overmind_node_get_dbupdate() {
  return array(
    'DBupdates pending' => _overmind_node_get_dbupdate_status(),
  );
}

/**
 * Function to check the status of update free access.
 *
 * @return string
 *   Unprotected or Protected, depending on the value of update_free_access.
 */
function _overmind_node_get_ufa_status() {
  $ret = 'Unprotected';
  if (empty($GLOBALS['update_free_access'])) {
    $ret = 'Protected';
  }
  return $ret;
}

/**
 * Function to check the status of database updates.
 *
 * @return array
 *   Unprotected or Protected, depending on the value of update_free_access.
 */
function _overmind_node_get_dbupdate_status() {
  $ret = array();
  // Check installed modules.
  foreach (module_list() as $module) {
    $updates = drupal_get_schema_versions($module);
    if ($updates !== FALSE) {
      $default = drupal_get_installed_schema_version($module);
      if (max($updates) > $default) {
        $ret[$module] = 'Needs schema updates';
      }
    }
  }
  return $ret;
}

/**
 * Function to gather module information.
 *
 * This code is from system.admin.inc, function system_modules()
 * @todo add @see
 * It is modified slightly to fit my purposes.
 *
 * @return array
 *   An array of modules and their metadata
 */
function _overmind_node_get_modules_status() {
  // Get current list of modules.
  $files = system_rebuild_module_data();

  // Remove hidden modules from display list.
  $visible_files = $files;
  foreach ($visible_files as $filename => $file) {
    if (!empty($file->info['hidden'])) {
      unset($visible_files[$filename]);
    }
  }

  $modules = array();

  // Used when displaying modules that are required by the install profile.
  require_once DRUPAL_ROOT . '/includes/install.inc';
  $distribution_name = check_plain(drupal_install_profile_distribution_name());

  // Iterate through each of the modules.
  foreach ($visible_files as $filename => $module) {
    $extra = array();
    $extra['enabled'] = (bool) $module->status;
    if (!empty($module->info['required'])) {
      $extra['disabled'] = TRUE;
      $extra['required_by'][] = $distribution_name . (!empty($module->info['explanation']) ? ' (' . $module->info['explanation'] . ')' : '');
    }

    // If this module requires other modules, add them to the array.
    foreach ($module->requires as $requires => $v) {
      if (!isset($files[$requires])) {
        $extra['requires'][$requires] = t('@module (<span class="admin-missing">missing</span>)', array('@module' => drupal_ucfirst($requires)));
        $extra['disabled'] = TRUE;
      }
      // Only display visible modules.
      elseif (isset($visible_files[$requires])) {
        $requires_name = $files[$requires]->info['name'];
        // Disable this module if it is incompatible with the dependency's version.
        if ($incompatible_version = drupal_check_incompatibility($v, str_replace(DRUPAL_CORE_COMPATIBILITY . '-', '', $files[$requires]->info['version']))) {
          $extra['requires'][$requires] = t('@module (<span class="admin-missing">incompatible with</span> version @version)', array(
            '@module' => $requires_name . $incompatible_version,
            '@version' => $files[$requires]->info['version'],
          ));
          $extra['disabled'] = TRUE;
        }
        // Disable this module if the dependency is incompatible with this
        // version of Drupal core.
        elseif ($files[$requires]->info['core'] != DRUPAL_CORE_COMPATIBILITY) {
          $extra['requires'][$requires] = t('@module (<span class="admin-missing">incompatible with</span> this version of Drupal core)', array(
            '@module' => $requires_name,
          ));
          $extra['disabled'] = TRUE;
        }
        elseif ($files[$requires]->status) {
          $extra['requires'][$requires] = t('@module (<span class="admin-enabled">enabled</span>)', array('@module' => $requires_name));
        }
        else {
          $extra['requires'][$requires] = t('@module (<span class="admin-disabled">disabled</span>)', array('@module' => $requires_name));
        }
      }
    }

    // If this module is required by other modules, list those, and then make it
    // impossible to disable this one.
    foreach ($module->required_by as $required_by => $v) {
      // Hidden modules are unset already.
      if (isset($visible_files[$required_by])) {
        if ($files[$required_by]->status == 1 && $module->status == 1) {
          $extra['required_by'][] = t('@module (<span class="admin-enabled">enabled</span>)', array('@module' => $files[$required_by]->info['name']));
          $extra['disabled'] = TRUE;
        }
        else {
          $extra['required_by'][] = t('@module (<span class="admin-disabled">disabled</span>)', array('@module' => $files[$required_by]->info['name']));
        }
      }
    }
    $data['modules'][$module->info['package']][$filename] = array('info' => $module->info, 'extra' => $extra);
  }

  return $data;
}

/**
 * Gets the timestamp of the last time cron was run.
 *
 * @return mixed array|bool
 *   FALSE if cron_last is non-numeric, otherwise an array with increments of
 *     time as keys and values for each key to represent the number of that
 *     increment of time.
 * @see _overmind_node_helper_sec2time
 */
function _overmind_node_get_cron_last_run() {
  // Report cron status.
  $cron_last = variable_get('cron_last');

  if (is_numeric($cron_last)) {
    $data = _overmind_node_helper_sec2time((int) (time() - $cron_last));
  }
  else {
    $data = FALSE;
  }
  return $data;
}

/**
 * Function to check how many rows are in the watchdog table in the database.
 *
 * @return int
 *   The number of rows in the watchdog table in the database.
 */
function _overmind_node_get_wd_rows() {
  return db_query('SELECT COUNT(*) FROM {watchdog}')->fetchField();
}

/**
 * Function to check how old the oldest watchdog entry is.
 *
 * @return array
 *   An array with increments of time as keys and values for each key to
 *     represent the number of that increment of time.
 * @see _overmind_node_helper_sec2time
 */
function _overmind_node_get_wd_oldest() {
  // (PHP 5 >= 5.3.0)
  // Timezone issues do not affect this since we are doing all time math on the
  // same server and only reporting the diff.
  $oldtime = db_query('SELECT `timestamp`,wid FROM {watchdog} ORDER BY wid ASC LIMIT 1')->fetchField();
  $time = time();
  $seconds = $time - $oldtime;
  return _overmind_node_helper_sec2time($seconds);
}

/**
 * Determines the number of various increments of time given a value of seconds.
 *
 * @param int $total_seconds
 *   The number of seconds to convert.
 *
 * @return array
 *   An array with increments of time as keys and values for each key to
 *     represent the number of that increment of time.
 */
function _overmind_node_helper_sec2time($total_seconds) {
  $ret = array(
    'years' => 0,
    'months' => 0,
    'days' => 0,
    'hours' => 0,
    'minutes' => 0,
    'seconds' => 0,
  );
  // Used in the while loop below to skip seconds.
  $seconds = 60;
  $minutes = 60;
  $hours = 3600;
  $days = 86400;
  $months = 2630000;
  $years = 31560000;

  foreach (array_keys($ret) as $item) {
    while ($total_seconds > $$item) {
      $ret[$item] = (int) floor($total_seconds / $$item);
      $total_seconds = $total_seconds - ($ret[$item] * $$item);
    }
  }
  $ret['seconds'] = (int) $total_seconds;
  return $ret;
}